<?php

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Door Lock Access - Log Aktivitas</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="shortcut icon" href="<?= base_url(); ?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>favicon.ico" type="image/x-icon">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/dist/css/AdminLTE.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="<?= base_url(); ?>component/dist/css/skins/skin-blue-light.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

</head>

<body class="hold-transition skin-blue-light sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/contain/header.php');

        if ($set == "log") {

        ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Log Aktivitas
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?= base_url(); ?>karyawan/log"><i class="fa fa-book"></i> Log Aktivitas</a></li>
                        <!-- <li class="active"></li> -->
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h1 class="box-title"></h1>
                                    <form action="<?= base_url(); ?>admin/log_chart" method="get">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="tanggal" class="form-control pull-right" id="reservation">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-danger">Submit</button>
                                        </div>
                                    </form>
                            

                                    <script>
                                        function selectChart(e) {
                                            if (e.value == 'figure_non_engineering_member') {
                                                document.getElementById('figure_non_engineering_member').style.display = '';
                                                document.getElementById('figure_access_per_employee_denied').style.display = 'none';
                                                document.getElementById('figure_access_per_employee_granted').style.display = 'none';
                                                document.getElementById('figure_access_per_room').style.display = 'none';
                                                document.getElementById('figure_type_of_access').style.display = 'none';
                                            } else {
                                                document.getElementById('figure_non_engineering_member').style.display = '';
                                                document.getElementById('figure_access_per_employee_denied').style.display = '';
                                                document.getElementById('figure_access_per_employee_granted').style.display = '';
                                                document.getElementById('figure_access_per_room').style.display = '';
                                                document.getElementById('figure_type_of_access').style.display = '';
                                            }
                                        }
                                    </script>
                                </div>
                                <figure id="figure_access_per_employee_granted" class="highcharts-figure">
                                    <div id="chart_granted_emp"></div>
                                </figure>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

        <?php
        }
        $this->load->view('admin/contain/footer.php');
        ?>

    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="<?= base_url(); ?>component/plugins/highcharts/highcharts.js"></script>
    <script src="<?= base_url(); ?>component/plugins/highcharts/highcharts-3d.js"></script>
    <script src="<?= base_url(); ?>component/plugins/highcharts/modules/exporting.js"></script>
    <script src="<?= base_url(); ?>component/plugins/highcharts/modules/export-data.js"></script>
    <script src="<?= base_url(); ?>component/plugins/highcharts/modules/accessibility.js"></script>

    <script src="<?= base_url(); ?>component/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= base_url(); ?>component/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?= base_url(); ?>component/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>component/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url(); ?>component/dist/js/adminlte.min.js"></script>

    <!-- date-range-picker -->
    <script src="<?= base_url(); ?>component/bower_components/moment/min/moment.min.js"></script>
    <script src="<?= base_url(); ?>component/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script type="text/javascript">
   
        var room_names = <?php echo json_encode($accessNonEnginnering_namaRoom); ?>;
        var series = <?php echo json_encode($accessNonEnginnering_series); ?>;

        Highcharts.chart('chart_non_engineering_member', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Frequently of Access Non Engineering Member'
            },
            xAxis: {
                categories: room_names,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Access Non Engineering Member '
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: series
        });
    </script>

</body>

</html>