<style>
  /* Fixed sidenav, full height */
.sidenav {
  height: 100%;
  width: 200px;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: white;
  overflow-x: hidden;
  padding-top: 20px;
}

/* Style the sidenav links and the dropdown button */
.sidenav a, .drpdwn-btn {
  font-weight: bold;
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  display: block;
  border: none;
  background: none;
  width:100%;
  text-align: left;
  cursor: pointer;
  outline: none;
}

/* On mouse-over */
.sidenav a:hover, .drpdwn-btn:hover {
  color: black;
  
}

.active {
  background-color: #f4f4f5;
  color: black;
}

/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-cntnr {
  display: none;
  padding-left: 8px;
}

.sub-menu {
  margin-left: 15px;
}
</style>
<header class="main-header">
  <!-- Logo -->
  <a href="#" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>AIO</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>AIO</b> System Lock</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= base_url(); ?>component/dist/img/admin/<?= $avatar; ?>" class="user-image" alt="User Image">
            <span class="hidden-xs">
              <?php if (isset($namauser)) {
                echo $namauser;
              } ?>
            </span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="<?= base_url(); ?>component/dist/img/admin/<?= $avatar; ?>" class="img-circle" alt="User Image">
              <p>
                <?php if (isset($namauser)) {
                  echo $namauser;
                } ?>
                <small>
                  <?php if (isset($role)) {
                    if ($role == 1) {
                      echo "Super Admin";
                    }
                    if ($role == 2) {
                      echo "Admin<br>Department ";
                      echo $this->session->userdata('nama_department');;
                    }
                  } ?>
                </small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="<?= base_url(); ?>admin/setting" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="<?= base_url(); ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>

      </ul>
    </div>
  </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url(); ?>component/dist/img/admin/<?= $avatar; ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php if (isset($namauser)) echo $namauser; ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>

      <div <?php if ($this->uri->segment(2) == "dashboard") echo 'class="active"'; ?>>
        <a class="drpdwn-btn" href="<?= base_url(); ?>admin/dashboard">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </div>
                
      <?php if ($this->session->userdata('control_room') == 1 || $role == 1) : ?>
        <div <?php if ($this->uri->segment(2) == "control") echo 'class="active"'; ?>>
          <a class="drpdwn-btn" href="<?= base_url(); ?>admin/control?page=1">
            <i class="fa fa-keyboard-o"></i> <span>Control Room</span>
          </a>
        </div>
      <?php endif; ?>

      <?php if ($this->session->userdata('monitoring_room') == 1 || $role == 1) : ?>
        <div <?php if ($this->uri->segment(2) == "monitoring") echo 'class="active"'; ?>>
          <a class="drpdwn-btn" href="<?= base_url(); ?>admin/monitoringdep?id_department=1">
            <i class="fa fa-window-restore"></i> <span>Monitoring Room</span>
          </a>
        </div>
      <?php endif; ?>

      <?php
      if (isset($role)) {
        if ($role == 1) {
      ?>
          <div <?php if ($this->uri->segment(2) == "department") echo 'class="active"'; ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/department">
              <i class="fa fa-building"></i> <span>Department</span>
            </a>
        </div>
          <div <?php if ($this->uri->segment(2) == "position") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/position"><i class="fa fa-bookmark"></i> Position</a>
            </div>
          <div <?php if ($this->uri->segment(2) == "list_admin") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/list_admin"><i class="fa fa-user"></i> Daftar Admin</a>
            </div>
          <div <?php if ($this->uri->segment(2) == "list_karyawan") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/list_karyawan"><i class="fa fa-users"></i> Daftar Karyawan</a>
            </div>
          <div <?php if ($this->uri->segment(2) == "list_room") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/list_room"><i class="fa fa-home"></i> Room</a>
            </div>
          <div <?php if ($this->uri->segment(2) == "free_access_room") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/free_access_room"><i class="fa fa-exchange"></i> Free Access Room</a>
            </div>
          
                  <?php if ($this->session->userdata('monitoring_room') == 1 || $role == 1) : ?>
                  <li <?php if ($this->uri->segment(2) == "monitoring") echo 'class="active"'; ?>>
                    <button  class="drpdwn-btn" href="<?= base_url(); ?>admin/monitoringdep?id_department=1">
                      <i class="fa fa-book"></i> <span>Log</span>
                    </button>
                    <div class="dropdown-cntnr">
                      <div <?php if ($this->uri->segment(2) == "log") {
                          echo 'class="active"';
                        } ?> >
                        <a class="sub-menu" href="<?= base_url(); ?>admin/log">
                          <i class="fa fa-table"></i> <span>Log Table</span>
                        </a>
                      </div>
                      <div <?php if ($this->uri->segment(2) == "log_chart") {
                          echo 'class="active"';
                        } ?>>
                        <a class="sub-menu" href="<?= base_url(); ?>admin/log_chart">
                          <i class="fa fa-chart"></i> <span>Log Chart</span>
                        </a>
                      </div>
                    </div>
                  </li>
                <?php endif; ?>

          <div <?php if ($this->uri->segment(2) == "notif") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/notif"><i class="<?= $this->uri->segment(2) == "control" ? "fab" : "fa"  ?> fa-telegram"></i> Notif </a>
            </div>
          <div <?php if ($this->uri->segment(2) == "notif-door") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/notif-door"><i class="fa fa-exclamation"></i> Notif Door Open </a>
            </div>
          <div <?php if ($this->uri->segment(2) == "setting") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/setting"><i class="fa fa-cog"></i> Setting</a>
            </div>
        <?php
        } else if ($role == 2) {
        ?>
          <div <?php if ($this->uri->segment(2) == "list_karyawan") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/list_karyawan"><i class="fa fa-users"></i> Daftar Karyawan</a>
            </div>
          <div <?php if ($this->uri->segment(2) == "list_room_dep") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/list_room_dep"><i class="fa fa-home"></i> Daftar Ruangan</a>
            </div>
          <div <?php if ($this->uri->segment(2) == "setting") {
                echo 'class="active"';
              } ?>>
            <a class="drpdwn-btn" href="<?= base_url(); ?>admin/setting"><i class="fa fa-cog"></i> Setting</a>
            </div>
      <?php
        }
      }
      ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

<script>
var dropdown = document.getElementsByClassName("drpdwn-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>