<?php

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Door Lock Access - Log Aktivitas</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="shortcut icon" href="<?= base_url(); ?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>favicon.ico" type="image/x-icon">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/dist/css/AdminLTE.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="<?= base_url(); ?>component/dist/css/skins/skin-blue-light.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?= base_url(); ?>component/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

</head>

<body class="hold-transition skin-blue-light sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/contain/header.php');

        if ($set == "log") {

        ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Log Aktivitas
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?= base_url(); ?>karyawan/log"><i class="fa fa-book"></i> Log Aktivitas</a></li>
                        <!-- <li class="active"></li> -->
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h1 class="box-title"></h1>
                                    <form action="<?= base_url(); ?>admin/log_status/<?= $room['id_room'] ?>" method="get">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                                <select name="period" class="form-control pull-right" id="reservation">
                                                    <option value="10">-- Jangka Waktu --</option>
                                                    <?php for ($i = 1; $i <= 100; $i++) : ?>
                                                        <option <?= ($this->input->get('period') == $i) ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-danger">Submit</button>
                                        </div>
                                    </form>
                                </div>
                                <figure class="highcharts-figure">
                                    <div id="container"></div>
                                </figure>

                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

        <?php
        }
        $this->load->view('admin/contain/footer.php');
        ?>

    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="<?= base_url(); ?>component/plugins/highcharts/highcharts.js"></script>
    <script src="<?= base_url(); ?>component/plugins/highcharts/highcharts-3d.js"></script>
    <script src="<?= base_url(); ?>component/plugins/highcharts/modules/exporting.js"></script>
    <script src="<?= base_url(); ?>component/plugins/highcharts/modules/export-data.js"></script>
    <script src="<?= base_url(); ?>component/plugins/highcharts/modules/accessibility.js"></script>

    <script src="<?= base_url(); ?>component/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= base_url(); ?>component/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?= base_url(); ?>component/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>component/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url(); ?>component/dist/js/adminlte.min.js"></script>

    <!-- date-range-picker -->
    <script src="<?= base_url(); ?>component/bower_components/moment/min/moment.min.js"></script>
    <script src="<?= base_url(); ?>component/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script type="text/javascript">
        var log_status = JSON.parse('<?= $log_status ?>')
        var time = JSON.parse('<?= $time ?>')

        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: '<?= $room["nama_room"] ?>'
            },

            xAxis: {
                categories: time
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: log_status
        });
    </script>

</body>

</html>