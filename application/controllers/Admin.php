<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->load->model('m_room');
		$this->load->library('bcrypt');
		date_default_timezone_set("asia/jakarta");

		if (!$this->session->userdata('userlogin')) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function index()
	{
		redirect(base_url() . 'admin/dashboard');
	}

	public function dashboard()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');
			$control_room = $this->session->userdata('control_room');
			$monitoring_room = $this->session->userdata('monitoring_room');

			$data['namauser'] = $namauser;
			$data['username'] = $username;
			$data['avatar'] = $avatar;
			$data['role'] = $role;
			$data['control_room'] = $control_room;
			$data['monitoring_room'] = $monitoring_room;

			$thismonth = strtotime(date("Y-m", strtotime('+0 month')));
			$nextmonth = strtotime(date('Y-m', strtotime('+1 month')));
			$data['note'] = "last 30 days";

			if (isset($_GET['tanggal'])) {
				$tanggal = $this->input->get('tanggal');
				//echo $tanggal;

				$split = explode("-", $tanggal);
				$x = 0;
				foreach ($split as $key => $value) {
					$date[$x] = $value;
					$x++;
				}

				$thismonth = strtotime($date[0]);
				$nextmonth = strtotime($date[1]);

				$data['note'] = date("d M Y", $thismonth) . " - " . date("d M Y", $nextmonth);

				$nextmonth += 86400;	// tambah 1 hari (hitungan detik)
			}

			$getRoomDashboard = $this->m_room->get_room_dashboard_active();
			$id_room_dash = 0;
			$nama_room_dash = "";
			if (isset($getRoomDashboard)) {
				foreach ($getRoomDashboard as $key => $value) {
					$id_room_dash = $value->id_room;
					$nama_room_dash = $value->nama_room;
				}
			}

			$flag2 = $this->db->get_where('room', ['flag_dashboard_dua' => 1])->row();
			$flag3 = $this->db->get_where('room', ['flag_dashboard_tiga' => 1])->row();
			$flag4 = $this->db->get_where('room', ['flag_dashboard_empat' => 1])->row();

			$exclude = $this->db->get_where('department', ['exclude' => 1])->row();

			$data['allRoom'] = $this->db->get_where('room', ['type' => 'public'])->result();
			$data['nama_room_dash'] = $nama_room_dash;
			$data['nama_room_dash_dua'] = $flag2->nama_room;
			$data['nama_room_dash_tiga'] = $flag3->nama_room;
			$data['nama_room_dash_empat'] = $flag4->nama_room;
			$data['nama_exclude'] = $exclude->nama_department;
			$data['dataAccess'] = $this->m_admin->getRoomByID($id_room_dash, $thismonth, $nextmonth, $exclude->id_department);
			$data['dataAccessDua'] = $this->m_admin->getRoomByID($flag2->id_room, $thismonth, $nextmonth, $exclude->id_department);
			$data['dataAccessTiga'] = $this->m_admin->getRoomByID($flag3->id_room, $thismonth, $nextmonth, $exclude->id_department);
			$data['dataAccessEmpat'] = $this->m_admin->getRoomByID($flag4->id_room, $thismonth, $nextmonth, $exclude->id_department);
			$data['department'] = $this->m_admin->get_department();
			$data['karyawan'] = $this->m_admin->getKaryawan();
			$data['room'] = $this->m_admin->getRoom();
			$data['totalAccess'] = $this->m_admin->getlogthismonth($thismonth, $nextmonth);

			$this->load->view('admin/v_dashboard', $data);
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function position()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['set'] = "position";
				$data['listdata'] = $this->m_admin->get_position();

				$this->load->view('admin/v_position', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function list_admin()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['set'] = "list-admin";
				$data['listdata'] = $this->m_admin->get_admin();

				$this->load->view('admin/v_listadmin', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function add_admin()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['listdepartment'] = $this->m_admin->get_department();
				$data['set'] = "add-admin";

				$this->load->view('admin/v_listadmin', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function save_admin()
	{
		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$department = $this->input->post('department');
				$nama = $this->input->post('nama');
				$username = $this->input->post('username');
				$email = $this->input->post('email');
				$pass = $this->input->post('pass');
				$hash = $this->bcrypt->hash_password($pass);
				$roleAdmin = 2;

				$type = explode('.', $_FILES["image"]["name"]);
				$type = strtolower($type[count($type) - 1]);
				$imgname = uniqid(rand()) . '.' . $type;
				$url = "component/dist/img/admin/" . $imgname;
				if (in_array($type, array("jpg", "jpeg", "gif", "png"))) {
					if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
						if (move_uploaded_file($_FILES["image"]["tmp_name"], $url)) {
							$data = array(
								'nama'    => $nama,
								'role'    => $roleAdmin,
								'username' => $username,
								'password' => $hash,
								'image'   => $imgname,
								'email'	  => $email,
								'id_department' => $department
							);
							$this->m_admin->insert_admin($data);
							$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
						}
					}
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Data gagal di simpan, ekstensi gambar salah</div>");
				}

				redirect(base_url() . 'admin/list_admin');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function hapus_admin($id = null)
	{
		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data = array('deleted' => 1);
				if ($this->m_admin->admin_update($id, $data)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
				}

				redirect(base_url() . 'admin/list_admin');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		}
	}

	public function edit_admin($id = null)
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				if (isset($id)) {
					$dataadmin = $this->m_admin->get_admin_by_id($id);
					$c = 0;
					if (isset($dataadmin)) {
						foreach ($dataadmin as $key => $value) {
							//print_r($value);
							$data['id_user'] = $value->id_user;
							$data['id_department'] = $value->id_department;
							$data['nama'] = $value->nama;
							$data['email'] = $value->email;
							$data['username'] = $value->username;
							$data['gambar'] = $value->image;
							$c++;
						}
						$data['listdepartment'] = $this->m_admin->get_department();
						$data['set'] = "edit-admin";
					}
					if ($c > 0) {
						$this->load->view('admin/v_listadmin', $data);
					} else {
						redirect(base_url() . 'admin/list_admin');
					}
				} else {
					redirect(base_url() . 'admin/list_admin');
				}
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		}
	}

	public function save_edit_admin()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				if (isset($_POST['id']) && isset($_POST['department'])) {
					$id = $this->input->post('id');
					$id_department = $this->input->post('department');
					$nama = $this->input->post('nama');
					$email = $this->input->post('email');
					$username = $this->input->post('username');

					$arrayData = array('id_department' => $id_department, 'nama' => $nama, 'email' => $email, 'username' => $username);

					$this->m_admin->admin_update($id, $arrayData);
					if ($this->m_admin->admin_update($id, $arrayData)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
					} else {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
					}

					if (isset($_POST['changepass'])) {
						$hash = $this->bcrypt->hash_password($username);
						$DataPass = array('password' => $hash);
						$this->m_admin->admin_update($id, $DataPass);
					}


					$type = explode('.', $_FILES["image"]["name"]);
					$type = strtolower($type[count($type) - 1]);
					$imgname = uniqid(rand()) . '.' . $type;
					$url = "component/dist/img/admin/" . $imgname;
					if (in_array($type, array("jpg", "jpeg", "gif", "png"))) {
						if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
							if (move_uploaded_file($_FILES["image"]["tmp_name"], $url)) {
								$dataGambar = array(
									'image'   => $imgname
								);
								$file = $this->input->post('img');
								$path = "component/dist/img/admin/" . $file;

								if (file_exists($path)) {
									unlink($path);
								}

								if ($this->m_admin->admin_update($id, $dataGambar)) {
									$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
								} else {
									$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
								}
							}
						}
					}
				}
				redirect(base_url() . 'admin/list_admin');
			}
		}
	}

	public function list_device_rfid()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['set'] = "list-device-rfid";
				$data['listdata'] = $this->m_admin->get_device_rfid();

				$this->load->view('admin/v_listdevicerfid', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function hapus_device_rfid($id = null)
	{
		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data = array('deleted' => 1);
				if ($this->m_admin->device_rfid_update($id, $data)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
				}

				redirect(base_url() . 'admin/list_device_rfid');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		}
	}

	public function add_device_rfid()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['listdepartment'] = $this->m_admin->get_department();
				$data['set'] = "add-device-rfid";

				$this->load->view('admin/v_listdevicerfid', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function save_device_rfid()
	{
		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$id_department = $this->input->post('id_department');

				$data = array(
					'deleted' => 0,
					'created_at' => time(),
					'data_rfid'   => '-',
					'status'	  => 0,
					'id_department' => $id_department
				);

				if ($this->m_admin->insert_device_rfid($data)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan</div>");
				}
				redirect(base_url() . 'admin/list_device_rfid');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function edit_device_rfid($id = null)
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				if (isset($id)) {
					$datadev = $this->m_admin->get_device_by_id($id);
					$c = 0;
					if (isset($datadev)) {
						foreach ($datadev as $key => $value) {
							//print_r($value);
							$data['id_device_rfid'] = $value->id_device_rfid;
							$data['id_department'] = $value->id_department;
							$c++;
						}
						$data['listdepartment'] = $this->m_admin->get_department();
						$data['set'] = "edit-device-rfid";
					}
					if ($c > 0) {
						$this->load->view('admin/v_listdevicerfid', $data);
					} else {
						redirect(base_url() . 'admin/list_device_rfid');
					}
				} else {
					redirect(base_url() . 'admin/list_device_rfid');
				}
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		}
	}

	public function save_edit_device_rfid()
	{
		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');
			if ($role == 1) {
				if (isset($_POST['id_device_rfid']) && isset($_POST['id_department'])) {
					$id = $this->input->post('id_device_rfid');
					$id_department = $this->input->post('id_department');

					$data = array('id_department' => $id_department);

					if ($this->m_admin->device_rfid_update($id, $data)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
					} else {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
					}
				}
				redirect(base_url() . 'admin/list_device_rfid');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		}
	}

	function searchForDept($val, $array)
	{
		foreach ($array as $key => $val) {
			if ($val['department'] === $val) {
				return $key;
			}
		}
		return null;
	}


	public function log_chart()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['set'] = "log";
				if ($this->input->get('tanggal')) {
					$tanggal = explode('-', $this->input->get('tanggal'));
					$first = new DateTime($tanggal[0]);
					$last = new DateTime($tanggal[1]);
					$firstx = strtotime($first->format('d-m-Y'));
					$lastx = strtotime($last->format('d-m-Y'));
				} else {
					$first = strtotime('today - 30 days');
					$last = strtotime('today');
					$firstx = strtotime('-30 days', strtotime('Today'));
					$lastx = strtotime('Today');
				}

				$access_per_room = $this->m_admin->get_access_per_room($firstx, $lastx);

				$data['access_per_room'] = json_encode($access_per_room);

				$data['access_per_employee'] = json_encode($this->m_admin->get_access_per_employee($firstx, $lastx));

				$accessPerEmployee = $this->m_admin->get_access_per_employee($firstx, $lastx);
				$karyawan = $this->m_admin->get_employees();

				$accPerEmp  = [];
				foreach ($karyawan as $key => $value) {
					$accPerEmp[] = [
						'nama_karyawan' => str_replace(["'"], '', $value['nama_karyawan']),
					];
				}
				$data['accPerEmp'] = json_encode($accPerEmp);

				$type_of_access = $this->m_admin->get_type_of_access($firstx, $lastx);
				$data['type_of_access'] = json_encode($type_of_access);


				$empDenied = [];
				$empGranted = [];
				$employees = $this->m_admin->get_employees();

				foreach ($employees as $ape) {
					$like = $ape['nama_karyawan'];
					$result = array_filter($accessPerEmployee, function ($item) use ($like) {
						if (stripos($item['nama_karyawan'], $like) !== false) {
							return true;
						}
						return false;
					});
					$Denied = 0;
					$Granted = 0;
					foreach ($result as $key => $value) {
						$ket = explode(',', $value['keterangan'])[0];
						if ($ket == 'Access Denied') {
							$Denied += 1;
						}
						if ($ket == 'Access Granted') {
							$Granted += 1;
						}
					}

					$key = array_search($ape['id_karyawan'], array_column($employees, 'id_karyawan'));
					if ($Denied != 0) {
						$empDenied[$ape['id_karyawan']] = [
							'nama_karyawan' => str_replace(["'"], '', $employees[$key]['nama_karyawan']),
							'total' => $Denied,
						];
					}
					if ($Granted != 0) {
						$empGranted[$ape['id_karyawan']] = [
							'nama_karyawan' => str_replace(["'"], '', $employees[$key]['nama_karyawan']),
							'total' => $Granted,
						];
					}
				}
				$granted = [];
				foreach ($empGranted as $key => $value) {
					$granted[] = $value;
				}
				$denied = [];
				foreach ($empDenied as $key => $value) {
					$denied[] = $value;
				}
				$data['denied_emp'] = json_encode($denied);
				$data['granted_emp'] = json_encode($granted);


				// $granted = [];
				// $denied = [];
				// foreach ($empDenied as $key => $value) {
				// 	$granted[] = $value['GRANTED'];
				// 	$denied[] = $value['DENIED'];
				// }
				// $data['granted'] = json_encode($granted);
				// $data['denied'] = json_encode($denied);

				foreach ($accessPerEmployee as $key => $value) {
					$employees[] = [
						'ket' => explode(',', $value['keterangan'])[0],
						'nama' => $value['nama_karyawan']
					];
				}

				$data['non_engineering_member'] = json_encode($this->m_admin->get_non_engineering_member($firstx, $lastx));
				$data['nama_room'] = json_encode($this->m_admin->get_room_enginnering());
				$engineeringRoom = $this->m_admin->get_room_enginnering();

				$employeeNonEngineering = $this->m_admin->get_karyawan_non_engineering($firstx, $lastx);
				$as = [];
				foreach ($engineeringRoom as $key => $value) {
					$like = $value['id_room'];
					$a = array_filter($employeeNonEngineering, function ($item) use ($like) {
						if (stripos($item['id_room'], $like) !== false) {
							return true;
						}
						return false;
					});
					$scmData = 0;
					$qaData = 0;
					$manData = 0;
					foreach ($a as $key => $val) {
						if ($val['nama_department'] == 'SCM') {
							$scmData += 1;
						} else if ($val['nama_department'] == 'QA') {
							$qaData += 1;
						} else {
							$manData += 1;
						}
					}
					$as[$value['id_room']] = [
						'id_room' => $value['id_room'],
						'SCM' => $scmData,
						'QA' => $qaData,
						'MAN' => $manData,
					];
				}

				$qaData = [];
				$scmData = [];
				$manData = [];
				foreach ($as as $key => $value) {
					if ($value['SCM'] != 0) {
						$scmData[] = $value['SCM'];
					} else {
						$scmData[] = 0;
					}
					if ($value['QA'] != 0) {
						$qaData[] = $value['QA'];
					} else {
						$qaData[] = 0;
					}
					if ($value['MAN'] != 0) {
						$manData[] = $value['MAN'];
					} else {
						$manData[] = 0;
					}
				}
				$data['qa'] = json_encode($qaData);
				$data['scm'] = json_encode($scmData);
				$data['man'] = json_encode($manData);

				// ===================================================================================
				// NON ENGINEERING ROOM
				$nem = $this->m_admin->get_nem($firstx, $lastx);

				$getRoomEnginnering = $this->m_admin->get_room_enginnering();

				// Assume that the data room and employee stored in variable $data_room and $data_employee

				// Create an empty array to store the data
				$finalData = array();

				// Create an array to store all department name
				$allDepartment = array();

				// Iterate over the data room
				foreach ($getRoomEnginnering as $room) {
					// Initialize the data for the room
					$finalData[$room['nama_room']] = array(
						'id_room' => $room['id_room'],
						'nama_room' => $room['nama_room'],
						'total_employee' => 0,
						'department' => array()
					);

					// Iterate over the data employee
					foreach ($nem as $employee) {
						// Check if the employee works in the room
						if ($employee['id_room'] == $room['id_room']) {
							// Increment the total employee for the room
							$finalData[$room['nama_room']]['total_employee']++;

							// Check if the department data exists for the room
							if (!isset($finalData[$room['nama_room']]['department'][$employee['nama_department']])) {
								// Initialize the department data for the room
								$finalData[$room['nama_room']]['department'][$employee['nama_department']] = array(
									'id_department' => $employee['id_department'],
									'nama_department' => $employee['nama_department'],
									'total_employee' => 0
								);
								$allDepartment[] = $employee['nama_department'];
							}
							// Increment the total employee for the department
							$finalData[$room['nama_room']]['department'][$employee['nama_department']]['total_employee']++;
						}
					}
				}

				// Remove duplicate department 
				$allDepartment = array_unique($allDepartment);

				// Iterate over each room
				foreach ($finalData as $roomName => $room) {
					// Iterate over each department
					foreach ($allDepartment as $department) {
						// Check if department already added on room
						if (!isset($room['department'][$department])) {
							//Initialize department on room
							$finalData[$roomName]['department'][$department] = array(
								'nama_department' => $department,
								'total_employee' => 0
							);
						}
					}
				}

				//remove data that not meet the condition
				$finalData = array_filter($finalData, function ($v) {
					return $v['total_employee'] != 0;
				});
				$nama_rooms = [];

				$accessNonEnginnering_series = array();
				foreach ($finalData as $room) {
					$nama_rooms[] = $room['nama_room'];
					foreach ($room['department'] as $department) {
						$department_name = $department['nama_department'];
						if (!array_key_exists($department_name, $accessNonEnginnering_series)) {
							$accessNonEnginnering_series[$department_name] = array(
								'name' => $department_name,
								'data' => array($department['total_employee'])
							);
						} else {
							array_push($accessNonEnginnering_series[$department_name]['data'], $department['total_employee']);
						}
					}
				}
				$accessNonEnginnering_series = array_values($accessNonEnginnering_series);
				$data['accessNonEnginnering_namaRoom'] = $nama_rooms;
				$data['accessNonEnginnering_series'] = $accessNonEnginnering_series;


				$this->load->view('admin/v_log_chart', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}
	public function log()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['karyawan'] = $this->db->get('karyawan')->result();
				$data['room'] = $this->m_room->get_room();
				$data['department'] = $this->m_admin->get_department();
				$data['remarks'] = $this->m_admin->get_remarks_log();

				if ($this->input->get('tanggal')) {
					$tanggal = $this->input->get('tanggal');
					//echo $tanggal;

					$split = explode("-", $tanggal);
					$x = 0;
					foreach ($split as $key => $value) {
						$date[$x] = $value;
						$x++;
					}

					$ts1 = strtotime($date[0]);
					$ts2 = strtotime($date[1]);

					$ts2 += 86400;	// tambah 1 hari (hitungan detik)
				}

				if ($this->input->get('id_karyawan')) {
					$logs = $this->db->where('log.id_karyawan', $this->input->get('id_karyawan'));
				}
				if ($this->input->get('id_room')) {
					$logs = $this->db->where('log.id_room', $this->input->get('id_room'));
				}
				if ($this->input->get('remark')) {
					$logs = $this->db->where('log.remarks_log', $this->input->get('remark'));
				}
				if ($this->input->get('id_department')) {
					$logs = $this->db->where('department_section.id_department', $this->input->get('id_department'));
				}
				$logs = $this->db->select('*');
				$logs = $this->db->from('log');
				$logs = $this->db->join('room', 'room.id_room=log.id_room', 'inner');
				$logs = $this->db->join('karyawan', 'karyawan.id_karyawan=log.id_karyawan', 'inner');
				$logs = $this->db->join('position', 'position.id_position=karyawan.id_position', 'inner');
				$logs = $this->db->join('department_section', 'department_section.id_section=karyawan.id_section', 'inner');
				$logs = $this->db->join('department', 'department.id_department=department_section.id_department', 'inner');
				$logs = $this->db->order_by('log.id_log', 'desc');
				if ($this->input->get('tanggal')) {
					# code...
					$logs = $this->db->where('log.access_time >=', $ts1);
					$logs = $this->db->where('log.access_time <', $ts2);
				}
				$logs = $this->db->limit(1000);
				$logs = $this->db->get()->result();

				$data['datalog'] = $logs;

				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;
				$data['set'] = "log";
				$this->load->view('admin/v_log', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function log_status($id_room)
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['karyawan'] = $this->db->get('karyawan')->result();
				$data['room'] = $this->m_room->get_room();
				$data['department'] = $this->m_admin->get_department();
				$data['remarks'] = $this->m_admin->get_remarks_log();
				$room = $this->db->get_where('room', ['id_room' => $id_room])->row_array();
				if($this->input->get('period'))
				{
					$period = $this->input->get('period');
				} else {
					$period = 10;
				}
				$get_log_status = $this->db->query('SELECT *
				FROM `log_status`
				WHERE `id_room` = ' . $id_room . ' AND `id_log_status` % ' . $period . ' = 0;
				')->result_array();

				$log_status = [
					[
						'name' => 'Open',
						'data' => []
					],
					[
						'name' => 'Auto',
						'data' => []
					],
					[
						'name' => 'Locksensor',
						'data' => []
					]
				];

				$time = [];

				foreach ($get_log_status as $row) {
					$log_status[0]['data'][] = (int) $row['open'] + 1;
					$log_status[1]['data'][] = (int) $row['auto'] + 3;
					$log_status[2]['data'][] = (int) $row['locksensor'] + 5;
					$time[] = date("Y-m-d H:i:s", $row['time']);
				}
				$data['log_status'] = json_encode($log_status);
				$data['time'] = json_encode($time);
				$data['room'] = $room;

				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;
				$data['set'] = "log";
				$this->load->view('admin/v_log_status', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}


	public function get_karyawan()
	{
		echo json_encode($this->db->get_where('karyawan', ['id_karyawan' => $this->input->post('id_karyawan')])->result_array()[0]);
	}

	public function setting()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1 || $role == 2) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['set'] = "setting";
				$data['secretKey'] = $this->m_admin->getSecretKey();
				$data['tokenTelegram'] = $this->m_admin->getTokenTelegram();
				$data['room_dashboard'] = $this->m_room->get_room_public();
				$data['departments'] = $this->db->get('department')->result();

				$this->load->view('admin/v_setting', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function free_access_room()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['set'] = "list-free-access-room";
				$data['listdata'] = $this->m_admin->get_free_access_room();

				$this->load->view('admin/v_freeaccessroom', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function add_free_access_room()
	{
		if ($this->session->userdata('userlogin')) {
			$namauser = $this->session->userdata('userlogin');
			$iduser = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$email = $this->session->userdata('email');
			$avatar = $this->session->userdata('image');
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$data['namauser'] = $namauser;
				$data['username'] = $username;
				$data['avatar'] = $avatar;
				$data['role'] = $role;

				$data['set'] = "add-free-access-room";
				$data['listkaryawan'] = $this->m_admin->getKaryawan();

				$this->load->view('admin/v_freeaccessroom', $data);
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function save_free_access_room()
	{
		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$id_karyawan = $this->input->post('id_karyawan');

				$getKaryawan = $this->m_admin->getFreeAccessRoombyIDKaryawan($id_karyawan);

				$x = 0;
				if (isset($getKaryawan)) {
					foreach ($getKaryawan as $key => $value) {
						$x++;
					}
				}

				if ($x == 0) {
					$data = array(
						'id_karyawan' => $id_karyawan
					);

					if ($this->m_admin->insert_free_access_room($data)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
					} else {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan</div>");
					}
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Karyawan sudah terdaftar Free Access Room</div>");
				}

				redirect(base_url() . 'admin/free_access_room');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function hapus_free_access_room($id = null)
	{
		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');

			if ($role == 1) {
				if ($this->m_admin->delete_free_access_room($id)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
				}

				redirect(base_url() . 'admin/free_access_room');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		}
	}

	public function downloadlog()
	{

		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');

			if ($role == 1) {
				// if (isset($_POST['tanggal'])) {


				if ($this->input->post('tanggal')) {
					$tanggal = $this->input->post('tanggal');
					//echo $tanggal;


					if ($this->input->post('tanggal')) {
						$tanggal = $this->input->post('tanggal');
						//echo $tanggal;

						$split = explode("-", $tanggal);
						$x = 0;
						foreach ($split as $key => $value) {
							$date[$x] = $value;
							$x++;
						}


						$ts1 = strtotime($date[0]);
						$ts2 = strtotime($date[1]);

						$ts2 += 86400;	// tambah 1 hari (hitungan detik)
					}


					$ts1 = strtotime($date[0]);
					$ts2 = strtotime($date[1]);

					$ts2 += 86400;	// tambah 1 hari (hitungan detik)
				}
				if ($this->input->post('id_karyawan')) {
					$datalog = $this->db->where('log.id_karyawan', $this->input->post('id_karyawan'));
				}
				if ($this->input->post('id_room')) {
					$datalog = $this->db->where('log.id_room', $this->input->post('id_room'));
				}
				if ($this->input->post('remark')) {
					$datalog = $this->db->where('log.remarks_log', $this->input->post('remark'));
				}
				if ($this->input->post('id_department')) {
					$datalog = $this->db->where('department_section.id_department', $this->input->get('id_department'));
				}
				$datalog = $this->db->select('*');
				$datalog = $this->db->from('log');
				$datalog = $this->db->join('room', 'room.id_room=log.id_room', 'inner');
				$datalog = $this->db->join('karyawan', 'karyawan.id_karyawan=log.id_karyawan', 'inner');
				$datalog = $this->db->join('position', 'position.id_position=karyawan.id_position', 'inner');
				$datalog = $this->db->join('department_section', 'department_section.id_section=karyawan.id_section', 'inner');
				$datalog = $this->db->join('department', 'department.id_department=department_section.id_department', 'inner');
				$datalog = $this->db->order_by('log.id_log', 'desc');
				if ($this->input->post('tanggal')) {
					$datalog = $this->db->where('log.access_time >=', $ts1);
					$datalog = $this->db->where('log.access_time <', $ts2);
				}
				$datalog = $this->db->limit(1000);
				$datalog = $this->db->get()->result();

				$spreadsheet = new Spreadsheet;
				$baris = 1;
				$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A1', 'No')
					->setCellValue('B1', 'Room')
					->setCellValue('C1', 'Nama')
					->setCellValue('D1', 'NIK')
					->setCellValue('E1', 'Position')
					->setCellValue('F1', 'Date')
					->setCellValue('G1', 'Time')
					->setCellValue('H1', 'Department')
					->setCellValue('I1', 'Section')
					->setCellValue('J1', 'Remarks')
					->setCellValue('K1', 'Keterangan');

				$baris++;
				$nomor = 1;

				if (isset($datalog)) {
					foreach ($datalog as $log) {

						$tgl = Date("d M Y", $log->access_time);
						$tm = Date("H:i:s", $log->access_time);

						$spreadsheet->setActiveSheetIndex(0)
							->setCellValue('A' . $baris, $nomor)
							->setCellValue('B' . $baris, $log->nama_room)
							->setCellValue('C' . $baris, $log->nama_karyawan)
							->setCellValue('D' . $baris, $log->nik)
							->setCellValue('E' . $baris, $log->position)
							->setCellValue('F' . $baris, $tgl)
							->setCellValue('G' . $baris, $tm)
							->setCellValue('H' . $baris, $log->nama_department)
							->setCellValue('I' . $baris, $log->nama_section)
							->setCellValue('J' . $baris, $log->remarks_log)
							->setCellValue('K' . $baris, $log->keterangan);

						$baris++;
						$nomor++;
					}
				}
				if ($this->input->post('tanggal')) {
					$nameDoc = $this->input->post('tanggal');
				} else {
					$nameDoc = '';
				}

				$writer = new Xlsx($spreadsheet);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="Log_' . $nameDoc . '.xlsx"');
				header('Cache-Control: max-age=0');

				$writer->save('php://output');
				// } else {

				// 	redirect(base_url() . 'admin/log');
				// }
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		}
	}
	public function set_room_dashboard()
	{
		if ($this->session->userdata('userlogin')) {
			$role = $this->session->userdata('role');

			if ($role == 1) {
				$room = $this->db->get_where('room', ['type' => 'public'])->result();

				if ($this->input->post('room_dash')) {
					foreach ($room as $rm) {
						$this->db->where('id_room', $rm->id_room);
						$this->db->update('room', ['flag_dashboard' => 0]);
					}

					$this->db->where('id_room', $this->input->post('room_dash'));
					$this->db->update('room', ['flag_dashboard' => 1]);
				}

				if ($this->input->post('room_dash_dua')) {
					foreach ($room as $rm) {
						$this->db->where('id_room', $rm->id_room);
						$this->db->update('room', ['flag_dashboard_dua' => 0]);
					}

					$this->db->where('id_room', $this->input->post('room_dash_dua'));
					$this->db->update('room', ['flag_dashboard_dua' => 1]);
				}

				if ($this->input->post('room_dash_tiga')) {
					foreach ($room as $rm) {
						$this->db->where('id_room', $rm->id_room);
						$this->db->update('room', ['flag_dashboard_tiga' => 0]);
					}

					$this->db->where('id_room', $this->input->post('room_dash_tiga'));
					$this->db->update('room', ['flag_dashboard_tiga' => 1]);
				}

				if ($this->input->post('room_dash_empat')) {
					foreach ($room as $rm) {
						$this->db->where('id_room', $rm->id_room);
						$this->db->update('room', ['flag_dashboard_empat' => 0]);
					}

					$this->db->where('id_room', $this->input->post('room_dash_empat'));
					$this->db->update('room', ['flag_dashboard_empat' => 1]);
				}

				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
				redirect(base_url() . 'admin/setting');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Area khusus super admin</div>");
				redirect(base_url() . 'login/admin');
			}
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-remove\"></i> Mohon Login terlebih dahulu</div>");
			redirect(base_url() . 'login/admin');
		}
	}

	public function exclude()
	{
		$departments = $this->db->get('department')->result();

		foreach ($departments as $department) {
			$this->db->where('id_department', $department->id_department);
			$this->db->update('department', ['exclude' => 0]);
		}

		$this->db->where('id_department', $this->input->post('exclude'));
		$this->db->update('department', ['exclude' => 1]);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
		redirect(base_url() . 'admin/setting');
	}
}
